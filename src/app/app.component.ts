import { Component } from '@angular/core';

@Component({
  selector: 'apollo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'picteste';
}
